import { Component, OnInit } from '@angular/core';
import { InsuranceService } from './service/insurance.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pageNumber: number = 1;
  pageSize: number = 100;
  allPage: number = 0;
  title = 'fs3';
  items = [];

  constructor(
    private insuranceService: InsuranceService
  ) { }

  ngOnInit() {
    this.getItems(1, this.pageSize);
    this.getAllPages();
  }

  changePage(pageNumber: number) {
    this.getItems(pageNumber, this.pageSize);
  }

  getItems(pageNumber: number, pageSize: number) {
    this.insuranceService.getAllPagination(pageNumber, pageSize).subscribe(data => {
      this.items = data;
    }, error => {
      console.log('error service', error);
    });
  }

  paginationPlus() {
    if (this.pageNumber <= this.allPage) {
      this.pageNumber = this.pageNumber + 1;
      this.changePage(this.pageNumber);
    }
  }

  paginationRest() {
    if (this.pageNumber > 1) {
      this.pageNumber = this.pageNumber - 1;
      this.changePage(this.pageNumber);
    }
  }

  pagination(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.changePage(this.pageNumber);
  }

  getAllPages() {
    this.insuranceService.getAllPages(this.pageSize).subscribe(data => {
      this.allPage = data;
    }, error => {
      console.log('error service', error);
    });
  }

  arrayOne(): any[] {
    return Array(this.allPage);
  }
}
