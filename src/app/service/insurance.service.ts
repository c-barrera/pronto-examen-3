import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InsuranceService {

  private url: string = 'http://localhost:8080/insurance';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<any>(this.url).pipe(map(data => data));
  }

  getAllPagination(pageNumber: number, pageSize: number) {
    return this.http.get<any>(`${this.url}/pagination/${pageNumber}/${pageSize}`).pipe(map(data => data));
  }

  getAllPages(pageSize: number) {
    return this.http.get<any>(`${this.url}/pages/${pageSize}`).pipe(map(data => data));
  }
}
